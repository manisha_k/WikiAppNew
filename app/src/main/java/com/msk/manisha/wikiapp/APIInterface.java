package com.msk.manisha.wikiapp;

import com.msk.manisha.wikiapp.models.Model;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface APIInterface {
    @GET("w/api.php?action=query&format=json&prop=pageimages%7Cpageterms")

    Call<Model> getResponse(
                            @Query("generator") String  generator,
                            @Query("redirects") String  redirect,
                            @Query("formatversion") String  formatversion,
                            @Query("piprop") String  piprop,
                            @Query("pithumbsize") String  pithumbsize,
                            @Query("pilimit") String  pilimit,
                            @Query("wbptterms") String  wbptterms,
                            @Query("gpssearch") String  gpssearch,
                            @Query("gpslimit") String  gpslimit
    );
}
